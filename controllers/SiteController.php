<?php

namespace app\controllers;

use app\models\Positions;
use yii\bootstrap\Html;
use yii\web\Controller;
use yii\web\Response;
use Yii;



class SiteController extends Controller
{

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $positions = Positions::find()->all();
        return $this->render('index',
            ['positions' => $positions]
        );
    }


    /**
     * Action ajax create new block
     *
     * @return array
     */
    public function actionCreate(){
        if (Yii::$app->request->isAjax) {
            $position = Positions::newPosition();
            Yii::$app->response->format = Response::FORMAT_JSON;
            return [
                "id" => $position->id,
                "x" => $position->position_x,
                "y" => $position->position_y,
                "width" => $position->width,
                "height" => $position->height,
            ];
        }
    }


    /**
     * Action ajax update current block
     *
     * @return bool
     */
    public function actionUpdate(){
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $post = Yii::$app->request->post();
            $position = $this->findPosition($post['id']);

            if($post['option'] === 'position'){
                $position->updatePosition($post['x'], $post['y']);
            }elseif ($post['option'] === 'size'){
                $position->updateSize($post['width'], $post['height']);
            }


            return true;
        }
    }

    /**
     * Action ajax delete block
     *
     * @return int
     * @throws \Exception
     * @throws \Throwable
     * @throws \yii\db\StaleObjectException
     */
    public function actionRemove(){
        if (Yii::$app->request->isAjax) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            $post = Yii::$app->request->post();
            $position = $this->findPosition($post['id']);
            if($position->delete())
                return $post['id'];
            return 0;
        }
    }


    /**
     * Find current position block
     * @param $position_id
     * @return array|null|Positions
     */
    protected function findPosition($position_id)
    {
        if (($position = Positions::find()->where(['id' => $position_id])->one()) !== null) {
            return $position;
        }
        return null;
    }
}
