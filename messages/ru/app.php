<?php
return [
    'Position X' => 'Позиция по X',
    'Position Y' => 'Позиция по Y',
    'Width' => 'Ширина',
    'Height' => 'Высота',
];