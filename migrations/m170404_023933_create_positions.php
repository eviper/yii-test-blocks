<?php

use yii\db\Migration;

class m170404_023933_create_positions extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('positions', [
            'id' => $this->primaryKey(),
            'position_x' => $this->smallInteger(4)->unsigned()->notNull(),
            'position_y' => $this->smallInteger(4)->unsigned()->notNull(),
            'width' => $this->smallInteger(4)->unsigned()->notNull(),
            'height' => $this->smallInteger(4)->unsigned()->notNull()
        ], $tableOptions);

        /** position_x */
        $this->createIndex(
            'idx_position_x',
            'positions',
            'position_x'
        );

        /** position_y */
        $this->createIndex(
            'idx_position_y',
            'positions',
            'position_y'
        );

        /** width */
        $this->createIndex(
            'idx_width',
            'positions',
            'width'
        );

        /** height */
        $this->createIndex(
            'idx_height',
            'positions',
            'height'
        );
    }

    public function safeDown()
    {
        $this->dropIndex('idx_height','positions');
        $this->dropIndex('idx_width','positions');
        $this->dropIndex('idx_position_y','positions');
        $this->dropIndex('idx_position_x','positions');
        $this->dropTable('positions');
    }


}
