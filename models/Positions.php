<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "positions".
 *
 * @property integer $id
 * @property integer $position_x
 * @property integer $position_y
 * @property integer $width
 * @property integer $height
 */
class Positions extends \yii\db\ActiveRecord
{

    const DEFAULT_X = 0;
    const DEFAULT_Y = 0;
    const DEFAULT_WIDTH = 100;
    const DEFAULT_HEIGHT = 100;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'positions';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['position_x', 'position_y', 'width', 'height'], 'required'],
            [['position_x', 'position_y', 'width', 'height'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'position_x' => Yii::t('app', 'Position X'),
            'position_y' => Yii::t('app', 'Position Y'),
            'width' => Yii::t('app', 'Width'),
            'height' => Yii::t('app', 'Height'),
        ];
    }

    /**
     * Change position in DB
     * @param $x
     * @param $y
     */
    public function updatePosition($x, $y){
        $this->position_x = $x;
        $this->position_y = $y;
        $this->save();
    }

    /**
     * Change size in DB
     * @param $width
     * @param $height
     */
    public function updateSize($width, $height){
        $this->width = $width;
        $this->height = $height;
        $this->save();
    }


    /**
     * Create new Position
     * @return Positions
     */
    public static function newPosition(){
        $position = new Positions();
        $position->position_x = Positions::DEFAULT_X;
        $position->position_y = Positions::DEFAULT_Y;
        $position->width = Positions::DEFAULT_WIDTH;
        $position->height = Positions::DEFAULT_HEIGHT;
        $position->save();
        return $position;
    }



}