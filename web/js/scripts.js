"use strict";

var draggable_opts = {
    stop: function() {
        var prepareData = {
            id: $(this).attr('id'),
            x: $(this).position().left,
            y: $(this).position().top,
            option: 'position'
        };

        update(prepareData);
    },
    containment: ".blocks",
    scroll: false
};

var resizable_opts = {
    stop: function() {
        var prepareData = {
            id: $(this).parent().attr('id'),
            width: $(this).width(),
            height: $(this).height(),
            option: 'size'
        };
        update(prepareData);
    }
};

/**
 * Update current block
 * Controller/action - site/update
 * @param prepareData
 */
function update(prepareData) {
    $.ajax({
        url: '/update',
        type: 'post',
        data: prepareData,
        success: function (data) {
        }
    });
}

/**
 * Create new block
 * Controller/action - site/create
 * @param id
 * @param x
 * @param y
 * @param width
 * @param height
 */
function create(id, x, y, width, height) {
    /** create wrap block */
    var block = $('<div/>', {
        id: id,
        class: 'block',
        style:
            'top: ' + y + 'px; ' +
            'left ' + x + 'px; ' +
            'width: ' + width + 'px; ' +
            'height: ' + height + 'px;' +
            'position: absolute'
    });
    block.draggable(draggable_opts);

    /** create block size*/
    var sizeBlock = $('<div/>', {class: 'block-size', style: 'width: 100%; height: 100%'});
    sizeBlock.resizable(resizable_opts);

    /** Create delete button */
    var a = $('<a/>', {href: '/'});
    a.on('click', function () {
        var data = {id: $(this).parent().attr('id')}
        $.ajax({
            url: 'remove',
            type: 'DELETE',
            data: data,
            success: function (id) {
                $('#'+id).remove();
            }
        })
    });


    /** build block */
    $('<span/>', {class: 'remove glyphicon glyphicon-remove'}).appendTo(a);

    a.appendTo(block);
    sizeBlock.appendTo(block);

    $('.blocks').append(block);
}



/** Init blocks */
(function ($) {
    $( ".block-size" ).resizable(resizable_opts);
    $( ".block" ).draggable(draggable_opts);
})(jQuery);
