<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $positions \app\models\Positions[] */

$this->title = 'Blocks';
?>

<div class="control-panel">
    <?= Html::button('Add block',
        [
            'class' => 'create btn btn-success',
            'onclick' =>"
                $.ajax({
                url: 'create',
                type: 'POST',
                success: function (data) {
                    console.log(data);
                    create(data.id, data.x, data.y, data.width, data.height)
                }
            })
        "])?>
</div>
<div class="blocks">
    <?php foreach ($positions as $position): ?>

        <div id="<?= $position->id ?>" class="block" style="
                top: <?= $position->position_y ?>px;
                left: <?= $position->position_x ?>px;
                width: <?= $position->width ?>px;
                height: <?= $position->height ?>px;">

            <?= Html::a('<span class="remove glyphicon glyphicon-remove" aria-hidden="true"></span>','/',[
                'onclick' =>"
                    var data = {id: $(this).parent().attr('id')}
                    $.ajax({
                        url: 'remove',
                        type: 'DELETE',
                        data: data,
                        success: function (id) {
                            $('#'+id).remove();
                        }
                })"
            ])?>
            <div class="block-size" style="width: 100%; height: 100%;">
            </div>

        </div>
    <?php endforeach; ?>

</div>
